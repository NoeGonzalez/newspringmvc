package com.softtek.academy.spring.beans;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "student")
public class Person {
	
	private Integer age;
	private String name;
	@Id
	private Integer id;
	
	public Integer getAge() {return age;}
	public void setAge(Integer age) {this.age = age;}
	public String getName() {return name;}
	public void setName(String name) {this.name = name;}
	public Integer getId() {return id;}
	public void setId(Integer id) {this.id = id;}
	
	
}
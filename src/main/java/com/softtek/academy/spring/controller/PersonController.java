package com.softtek.academy.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.softtek.academy.spring.beans.Person;
import com.softtek.academy.spring.dao.PersonDaoImpl;
import com.softtek.academy.spring.service.PersonService;


@Controller
public class PersonController {
	
	@Autowired
	PersonService personService;
    
	//Recuerda borrar este dao, es del proyecto anterior
	PersonDaoImpl dao;//will inject dao from xml file
	
	//Llegar al formulario de Person
	@RequestMapping(value="/person", method=RequestMethod.GET)
	public ModelAndView student() {
		return new ModelAndView("person", "command", new Person());
	}
	
	//Agregar usuario basándose en los atributos
	@RequestMapping(value = "/addPerson", method = RequestMethod.POST)
    public String addPerson(@ModelAttribute("SpringWeb")Person person, ModelMap model) {
        model.addAttribute("name", person.getName());
        model.addAttribute("age", person.getAge());
        model.addAttribute("id", person.getId());
        personService.save(person);
        model.addAttribute("command", new Person());
        
        return "person";   
    }
	
	//---------------------------------------------------------------------------------------
	
	//Mostrar Todos los Estudiantes
	@RequestMapping(value="/showPersons", method = RequestMethod.GET)
	public String persons(@ModelAttribute("SpringWeb") Person person, ModelMap model){
		System.out.println(personService.getAll());
		model.addAttribute("list", personService.getAll());		
		return "showPersons";
	}
	
	//---------------------------------------------------------------------------------------
	
    /* It deletes record for the given id in URL and redirects to /viewemp */
    @RequestMapping(value="/deletePerson/{id}",method = RequestMethod.GET)
    public String delete(@ModelAttribute("SpringWeb") Person person, ModelMap model,@PathVariable int id){
        personService.delete(id);
		System.out.println(personService.getAll());
		model.addAttribute("list", personService.getAll());		
		return "showPersons";
    }

	//---------------------------------------------------------------------------------------
	
	//Conseguir un estudiante en específico
	@RequestMapping(value="/getPerson/{id}", method = RequestMethod.GET)
	public ModelAndView getPerson(@ModelAttribute("SpringWeb") Person person, ModelMap model, @PathVariable int id) {
		Person p = personService.getById(id);

    	
		return new ModelAndView("editPerson", "command", p);
		
	}
	
    @RequestMapping("/modifyPerson")
    public String showform(@ModelAttribute("SpringWeb") Person person, ModelMap m){
        personService.update(person);
		m.addAttribute("list", personService.getAll());	
        return "showPersons";
    }

    /* It provides list of employees in model object */
    
    /*
    @RequestMapping("/showPersons")
    public String viewemp(Model m){
        List<Person> list = dao.getEmployees();
        m.addAttribute("list",list);
        return "showPersons";
    }
    */
    
    /* It displays object data into form for the given id.
     * The @PathVariable puts URL data into variable.*/
    /*
    @RequestMapping(value="/editemp/{id}")
    public String edit(@PathVariable int id, Model m){
        Person emp=dao.getById(id);
        m.addAttribute("command",emp);
        return "empeditform";
    }
    */
    /* It updates model object. */
    /*
    @RequestMapping(value="/editsave",method = RequestMethod.GET)
    public String editsave(@ModelAttribute("emp") Person emp){
        dao.update(emp);
        return "redirect:/viewemp";
    }
    */
	
}

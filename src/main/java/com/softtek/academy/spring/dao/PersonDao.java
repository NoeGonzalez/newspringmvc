package com.softtek.academy.spring.dao;

import java.util.List;

import com.softtek.academy.spring.beans.Person;

public interface PersonDao {
	public List<Person> getAll();
	public void save(Person p);
	public Person getById(int id);
	public void delete (int p);
	public void update (Person p);
}

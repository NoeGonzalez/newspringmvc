package com.softtek.academy.spring.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.softtek.academy.spring.beans.Person;

public class PersonDaoImpl implements PersonDao{
	private JdbcTemplate jdbcTemplate;   
	   
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {   
	    this.jdbcTemplate = jdbcTemplate;   
	}   
	   
	public int saveStudent(Person e){   
	    String query="insert into student values('"+e.getName()+"','"+e.getAge()+"','"+e.getId()+"')";   
	    return jdbcTemplate.update(query); 
	}
	
	/*
	public int update(Person p){
        String sql="update Person set name='"+p.getName()+"', age="+p.getAge()+"' where id="+p.getId()+"";
        return jdbcTemplate.update(sql);
    }
    */
	
	/*
   
    public int delete(int id){
        String sql="delete from student where id="+id+"";
        return jdbcTemplate.update(sql);
    }
   */
    public Person getById(int id){
        String sql="select * from student where id=?";
        return jdbcTemplate.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<Person>(Person.class));
    }
   
    public List<Person> getEmployees(){
        return jdbcTemplate.query("select * from student",new RowMapper<Person>(){
            public Person mapRow(ResultSet rs, int row) throws SQLException {
                Person e = new Person();
                e.setName(rs.getString(1));
                e.setAge(rs.getInt(2));
                e.setId(rs.getInt(3));
                return e;
            }
        });
    }

	@Override
	public List<Person> getAll() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void update(Person p) {
		// TODO Auto-generated method stub
	}

	@Override
	public void save(Person p) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(int p) {
		// TODO Auto-generated method stub
		
	}
}

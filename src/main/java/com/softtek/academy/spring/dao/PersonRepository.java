package com.softtek.academy.spring.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.softtek.academy.spring.beans.Person;

@Repository("personRepository")
public class PersonRepository implements PersonDao{

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public List<Person> getAll() {
		return entityManager.createQuery("Select P FROM Person P", Person.class).getResultList();
	}

	@Override
	public Person getById(int id) {
		return entityManager.find(Person.class, id);
	}

	@Override
	public void delete(int n) {
		Person p = entityManager.find(Person.class, n);
		entityManager.remove(p);
	}

	@Override
	public void update(Person p) {
		p = entityManager.merge(p);
	}

	@Override
	public void save(Person p) {
		entityManager.persist(p);
		
	}
}

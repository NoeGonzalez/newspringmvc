package com.softtek.academy.spring.service;

import java.util.List;

import com.softtek.academy.spring.beans.Person;

public interface PersonService {
	public List<Person> getAll();
	public Person getById(int id);
	public void save(Person p);
	public void delete (int id);
	public void update (Person p);
}

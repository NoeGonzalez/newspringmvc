package com.softtek.academy.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.softtek.academy.spring.beans.Person;
import com.softtek.academy.spring.dao.PersonDao;

@Transactional
@Service
public class PersonServiceImpl implements PersonService {
	
	@Qualifier("personRepository")
	@Autowired
	private PersonDao personDao;
	
	@Override
	public List<Person> getAll() {
		// TODO Auto-generated method stub
		return personDao.getAll();
	}

	@Override
	public Person getById(int id) {
		// TODO Auto-generated method stub
		return personDao.getById(id);
	}

	@Override
	public void delete(int p) {
		// TODO Auto-generated method stub
		personDao.delete(p);
		
	}

	@Override
	public void update(Person p) {
		// TODO Auto-generated method stub
		personDao.update(p);
	}

	@Override
	public void save(Person p) {
		personDao.save(p);
	}
	
}
